/* Copyright 2021 Dominik George <dominik.george@teckids.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

const BASE_NAME: &str  = "system_auth_webapi";

// Modules and macro imports for our own code
#[macro_use] extern crate log;
mod unix;
mod cache;
mod logging;
mod config;
mod oauth;

// Module and macro imports for the PAM component
#[macro_use] extern crate pamsm;
mod pam;

// Module and macro imports for the NSS component
extern crate libc;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate libnss;
mod nss;
